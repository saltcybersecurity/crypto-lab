# Cryptografie

## Inhoudsopgave
1. [Tools](#markdown-header-tools)
2. [Installatie](#markdown-header-installatie)
3. [Opdrachten](#markdown-header-opdrachten)

## Tools
1. Command Line interface (Terminal, Command line, PowerShell).
2. Putty of SSH (Voor connectie met machine).

## Installatie
Voor deze workshop staat er een omgeving klaar waarop de opdrachten uitgevoerd worden. De hostname en PEM file van deze omgeving zal van te voren gedeeld worden. Op de omgeving is de Kali Linux Shell klaar gezet.

Volg de volgende stappen om connectie te maken met deze machine:

#### MacOS / Linux
Pas de rechten van de file aan naar 400 d.m.v. `chmod 400 [pem file]`.
Gebruik van SSH voor de connectie met de machine `ssh -i '[pem file]' kali@[machine adres]`.

#### Windows
In Putty maak een nieuwe hostname aan. Gebruik de meegstuurde PPK file en selecteer die bij SSH / Auth. Klik op 'open' om de connectie tot stand te brengen. Inloggen met user 'kali'.


#### Update van de omgeving
Draai deze commando's, zodat de machine up to date is:

* `sudo apt update`
* `sudo apt install -y kali-linux-headless`

## Opdrachten
Voor deze workshop hebben we een zestal opdrachten klaar staan. Tijdens deze opdrachten zal je zelf aan de slag gaan met het ontcijferen van versleutelde tekst, kraken van wachtwoorden en het versleutelen van berichten. De opdrachten dienen in de aangegeven volgorde voltooid te worden.

## 1. Encryptie
Encryptie is het versleutelen van data zodat deze niet gelezen kan worden door een andere partij. Dit wordt veel al gedaan d.m.v. een algoritme die bepaalt hoe de versleutelde data eruit ziet. Alleen door het hebben of weten van de key kan een tweede partij de data weer ontsleutelen (decrypten). Door de eeuwen heen is encryptie steeds belangrijker geworden voor het beschermen van data.

### 1.1. Ciphers
Ciphers, ook wel coderingsalgoritmen genoemd, zijn algoritmes voor het coderen en decoderen van gegevens. Een cipher zet het originele bericht, de zogenaamde platte tekst, om in ciphertext met behulp van een sleutel om te bepalen hoe het wordt gedaan.

### 1.2. Ceasar Cipher
Het Ceaser Cipher is één van de eerste vormen van encryptie. Bij deze vorm wordt elke letter een aantal plaatsen opgeschoven in het alphabet. Het aantal plaatsen wordt hierbij bepaald door een offset. Als voorbeeld bij een offset van 1 worden alle letters 1 plek opgeschoven naar rechts, 'A' -> 'B' en alle 'R' -> 'S'. Deze offset kan positief en negatief zijn.

Voor de volgende drie opdrachten kan je gebruik maken van verschillende tools die beschikbaar zijn online, maar probeer eerst zelf om deze opdrachten uit te voeren zonder hulp.

Tip: kijk naar simpele of korte woorden in de zin, zoals 1, 2 of 3 letters. Bijvoobeeld bij een offset van 1: aan -> bbo. 

#### 1.2a. Probeer het volgende Ceaser Cipher te decrypten.
Ibmmp eju jt tbmu tfdvsjuz

#### 1.2b. Door middel van een random offset wordt het lastiger voor een aanvaller om deze code te decrypten. Probeer het volgende Ceaser Cipher te decrypten.
Iny nx jjs qfsl Hjfxjw Hnumjw

#### 1.2c. Een offset kan ook negatief zijn zoals bij het volgende bericht.
Chs hr ddm mdfzshdud rghes

![White space](/readme-images/white.png)

## 2. Hashing
Hashing is het proces waarbij een data set door middel van een algoritme wordt veranderd in een andere data set. Deze actie is niet meer omkeerbaar. De lengte van een hash value wordt bepaald door het type algoritme, meestal is een hash hierdoor korter dan de data die als input gebruikt wordt. 

### 2.1. MD5
Het MD5 message-digest-algoritme is een cryptografisch gebroken maar nog steeds veel gebruikte hashfunctie die een 128-bit hashwaarde produceert. Hoewel MD5 oorspronkelijk was ontworpen om te worden gebruikt als een cryptografische hash-functie, bleek het te lijden aan uitgebreide kwetsbaarheden. Het kan nog steeds worden gebruikt als controlesom om de gegevensintegriteit te verifiëren, om weerstand te bieden tegen corruptie. Het blijft dus geschikt voor andere niet-cryptografische doeleinden, zoals het berekenen van een checksum van bestanden om te garanderen dat een bestand niet gewijzigd is.


### 2.2. Secure Hash Algorithms (SHA)
De Secure Hash Algorithms zijn een familie van cryptografische hashfuncties die zijn gepubliceerd door het National Institute of Standards and Technology (NIST) als een Amerikaanse Federal Information Processing Standard (FIPS, zie FIPS 140), waaronder SHA-0, een retroniem toegepast op de originele versie van de 160-bit hashfunctie gepubliceerd in 1993 onder de naam "SHA". [Klik hier voor meer informatie over SHA](https://nl.wikipedia.org/wiki/SHA-familie).

### 2.3. Aanmaken hashes voor MD5 en SHA
Bedenk een getal tussen de 0 en 999999. Dit getal gaan we daarna hashen door middel van MD5, SHA-1 en SHA-256. De hash waarde die hieruit komt zullen we daarna door middel van Hashcat proberen te brute forcen. 

Met de volgende commando's kunnen de hashes aangemaakt worden:

* Voor MD5: `echo -n [string] | md5sum`.
* Voor SHA-1: `echo -n [string] | sha1sum`.
* Voor SHA-256: `echo -n [string] | sha256sum`.

Mocht je deze direct willen opslaan in een bestand dan is het raadzaam om de volgende commando's te gebruiken:

* Voor MD5: `echo -n [string] | md5sum|tr -d '-'|tr -d '[:space:]'>> bestandsnaam.extensie`.
* Voor SHA-1: `echo -n [string] | sha1sum|tr -d '-'|tr -d '[:space:]'>> bestandsnaam.extensie`.
* Voor SHA-256: `echo -n [string] | sha256sum|tr -d '-'|tr -d '[:space:]'>> bestandsnaam.extensie`.

### 2.4. Hashcat
Hashcat is een veelzijdige pentesttool die helpt bij het bruteforcen van de hashwaardes van wachtwoorden door middel van gokken of toepassen van lijsten. Tijdens het pentesten kan het makkelijk te raden credentials blootleggen.

Een aantal flags die veel worden gebruikt zijn:

* `-a`: Dit geeft aan welke type aanval we gebruiken, 3 staat voor brute force
* `-m`: Dit geeft het hash algoritme aan, 0 = MD5, 100 = SHA1, 1400 = SHA256
* `-o`: Dit zorgt ervoor dat er een file wordt aangemaakt met daarin de resulaten van Hashcat
* `--increment`: Dit geeft aan dat je hashcat de operatie meerdere keren laat uitvoeren met een minimale en/of maximale woordlengte. Dit geef je aan met --increment-min/max
* `--increment-min`: Dit geeft de minimale woordlengte aan
* `--increment-max`: Dit geeft de maximale woordlengte aan
* `?d?d?d`: Dit geeft de charset aan die gebruikt wordt in een bruteforce-attack (-a3), let op want de hoeveelheid combinaties zorgt voor de maximale woordlengte, ?u?u?u betekend dat er maximaal 3 tekens worden gebruikt en elk teken bestaat uit een hoofdletter uit het alfabet

Charset
In de bruteforce attacks kun je verschillende charsets meegeven. Dit zijn vooraf gedefineerde tekens in een set.
Deze sets zijn:

* `?l`: alfabet kleine letters
* `?u`: alfabet hoofdletters
* `?d`: cijfers 0-9
* `?h`: cijfers 0-9 & alfabet kleine letters
* `?H`: cijfers 0-9 & alfabet hoofdletters
* `?s`: alle speciale tekens
* `?a`: ?l?u?d?s
* `?b`: hexadecimaal

Hiervan is de volgorde en de hoeveelheid die je in het commando zet van belang.
`?l?l?d?u` houdt in dat er eerst een kleine letter staat, dan nog 1 kleine letter, daarna een cijfer en dan als laatste een hoofdletter.

Als je een onbekende hashlengte hebt of een indicatie van hoe groot een wachtwoord mini- en maxi-maal is dan kun je de –increment switch toevoegen. Dit zorgt ervoor dat Hashcat met een minimale en/of maximale woordlengte wordt getest. Van belang is dat je voldoende charsets toevoegt anders stopt hashcat na het opgegeven aantal charsets.

Voorbeeld:
Je weet dat een wachtwoord tussen de 1 en 4 cijfers bevat. In plaats van 4x hashcat te laten draaien kun je het volgende commando gebruiken:
`hashcat -a3 -m0 --increment --increment-min=1 --increment-max=4 target.txt[of hash] ?d?d?d?d`
Let op. Je gebruikt hier altijd een andere attackmodus dan `-a0` want je gebruikt hier `-a3`. Dit is een bruteforce attack. Daarnaast is het van belang dat je genoeg charsets meegeeft. Als je maar 3 tekens meegeeft `?d?d?d` dan gaat hashcat niet verder dan 3 tekens.

#### 2.4a. Probeer door middel van Hashcat de MD5 hash te kraken die je bij 2.4 gegenereerd hebt.
`hashcat -a 3 -m 0 [md5 hash] --increment --increment-min=1 --increment-max=6 ?d?d?d?d?d?d`

#### 2.4b. Probeer door middel van Hashcat de SHA-1 hash te kraken die je bij 2.4 gegenereerd hebt.
`hashcat -a 3 -m 100 [sha1 hash] --increment --increment-min=1 --increment-max=6 ?d?d?d?d?d?d`

#### 2.4c. Probeer door middel van Hashcat de SHA-256 hash te kraken die je bij 2.4 gegenereerd hebt.
`hashcat -a 3 -m 1400 [sha256 hash] --increment --increment-min=1 --increment-max=6 ?d?d?d?d?d?d`

Wat je hopelijk hebt opgemerkt is dat de SHA-256 vele malen langer duurt dan die van MD5 en SHA-1.

![White space](/readme-images/white.png)


## 3. Salt
Zoals je bij stap 2 hopelijk hebt kunnen zien is niet elk hash algoritme waterdicht, in tijd word elke algoritme gekraakt en verschijnen er rainbow tables die het hackers mogelijk maken om jouw wachtwoord te vinden. Om je hier beter tegen te beschermen is Salt bedacht. Salt is een random stukje data wat bij een wachtwoord wordt toegevoegd voordat het gehashed wordt. Deze Salt waarde wordt vaak in de database opgeslagen naast de gehashde versie.

Een Salt werk als volgd:
* Voor een string: `echo -n [wachtwoord][random string] | sha256sum`.


![White space](/readme-images/white.png)
## 4. Symmetrische encryptie
Bij symmetrische encryptie wordt gebruik gemaakt van 1 key, die data kan encrypten en ook weer kan decrypten. Deze manier van encryptie is vele malen sneller dan asymmetrische encryptie omdat er geen keys over en weer gestuurd hoeven te worden. Denk bij symmetrische encryptie bijvoorbeeld aan het encrypten van je harde schijf of grote bestanden.

Wat extreem belangrijk is bij symmetrische encryptie is de veiligheid van de gedeelde key. Wanneer iemand anders deze kan achterhalen kan deze gemakkelijke alle data decrypten. Zorg er daarom voor dat deze keys goed worden opgeslagen, bijvoorbeeld in een vault.

#### 4a. Aanmaken van een symmetrische key.
Voor deze oefening gaan we door middel van openssl een symmetrische key aanmaken.
Let op, dit is een versimpeld voorbeeld van een symmetrische key, gebruik die niet voor productie doeleinden.

Commando voor het genereren van een symmetrische key:

`openssl rand -out testkey.bin 32`.

* rand: Commando voor output random output.
* -out: Naam van de key.
* 32: Aantal bytes voor iteratie.


#### 4b. Encrypten van een bestand.
Maakt een random txt bestand aan met hierin wat tekst / data. Dit bestand gaan we encrypten door middel van de key die we hiervoor hebben aangemaakt.

Commando voor encrypten van het bestand door middel van de symmetrische key:

`openssl enc -aes-128-cbc -in [bestandnaam] -k file:testkey.bin -out encrypted.txt`.

* enc: Cipher commando voor het encrypten en decrypten van data.
* -aes-128-cbc: Het cipher wat we gebruiken voor de encryptie. Het kan zijn dat dit cipher niet meer aangeboden wordt, kies een ander cipher d.m.v. `openssl enc --help`.
* -in: De file die encrypt gaat worden.
* -k: De key die we gebruiken om het mee te versleutelen, deze is ook nodig voor het decrypten.
* -out: De encrypted file.

#### 4c. Decrypten van een bestand.
Als je dit bestand nu probeerd te openen valt het niet te lezen. Daarom kan je het ook weer decrypten met dezelfde key die gebruikt is voor het encrypten van dit bestand.

Commando voor decrypten van het bestand door middel van de symmetrische key:

`openssl enc -aes-128-cbc -d -in encrypted.txt -k file:testkey.bin -out decrypted.txt`.

* enc: Cipher commando voor het encrypten en decrypten van data.
* -aes-128-cbc: Het cipher wat we gebruiken voor de decryptie.
* -d: Hiermee geven we aan dat het gaan om decryptie.
* -in: De file die decrypt gaat worden.
* -k: De key die we gebruiken om het mee te decrypten.
* -out: De decrypted file.


![White space](/readme-images/white.png)
## 5. Asymmetrische encryptie
Public-key cryptography, oftwel asymmetric cryptografie is een cryptografie systeem waar gebruik wordt gemaakt van key pairs. Elk key paar bestaat uit een public en private key. De keys worden gegenereerd door middel van een cryptografisch algoritme.

De flow voor het encrypten van een data bestand:

![White space](/readme-images/encryptie.png)

### Private Key
Een private key is iets wat van jouwzelf is en niet gedeeld mag worden met andere. Deze key wordt gebruikt voor het decrypten van data en het signen van data.

#### 5a. Aanmaken van je eigen private key.
Voor deze opdracht gebruiken we openssl voor het aanmaken van de private en public keys.

Commando voor het aanmaken van de private key:

`openssl genrsa -out private-[naam].pem`

* genrsa: Commando voor het genereren van een private RSA key.
* -out: wegschrijven van de file.


### Public Key
Een public key is een key die door iedereen gebruikt kan worden. Wanneer je data met een public key van iemand encrypt kan alleen diegene de data weer decrypten met zijn private key.

#### 5b. Aanmaken van je eigen public key en deze delen met de personen die jouw berichten mogen sturen.
Voor deze opdracht maken we door middel van onze private key een public key aan. Deze key kunnen we dan delen met andere mensen. Voor dit scenario mogen de keys gedeeld worden in de meeting room. Je kunt iemand zijn public key dan gebruiken om een encrypted message naar die persoon te sturen.

Commando voor het aanmaken van de public key:

`openssl rsa -in private-[naam].pem -pubout -out public-[naam].pem`

* rsa: Process voor het verwerken van RSA keys.
* -in: Input van de key.
* -pubout: Forceren van een public key.
* -out: wegschrijven van de file.

Zorg dat je de public key van de omgeving naar je de shared machine kopieert. Dit kan je doen door middel van het volgende commando:

`scp public-[naam].pem crypto@ec2-54-74-245-236.eu-west-1.compute.amazonaws.com:/home/crypto/pki`


#### 5c. Maak een berichtje en encrypt die met een ander zijn public key.
Gebruik 1 van de gedeelde public keys om een ecrypted message te maken. Schrijf een stukje tekst in een data.txt bestand en probeer het daarna te encrypten door middel van openssl en de public key. Stuur daarna die bestandje door naar de ontvanger.

Als je het bericht als plain text wil delen in de chat is het belangrijk dit eerst base64 te encoden. Dit kan met het volgende commando: `base64 file`. De ontvanger dient het dan eerst te decoden d.m.v. `base64 -d file `.

Commando voor het encrypten van data:

`openssl rsautl -encrypt -pubin -inkey public-[ontvanger].pem -in data.txt -out [ontvanger].txt.enc`

* rsautl: Commando voor het signen / veriferen / en crypten en decrypten van data.
* -encrypt: Commando voor het encrypten.
* -inkey: De key file die gebruikt wordt voor het encrypten (default private key). In dit geval de public key van de ontvanger.
* -pubin: Aangeven dat de input een public RSA key is.
* -in: Input van file name.
* -out: wegschrijven van de file.

Kopieer het bestand naar de shared machine om voor iemand een bericht achter te laten:

`scp [ontvanger].txt.enc crypto@ec2-54-74-245-236.eu-west-1.compute.amazonaws.com:/home/crypto/messages`

#### 5d. Decrypt een encrypted bericht.
Laat een mede cursist bij stap 5c een bericht voor jou encrypten met jouw public key. Dit bericht kan je dan bij deze stap proberen te decrypten door middel van jouw private key.

Commando voor het decrypten van data:

`openssl rsautl -decrypt -inkey private-[naam].pem -in [ontvanger].txt.enc -out data.txt`

* rsautl: Commando voor het signen / veriferen / en crypten en decrypten van data.
* -decrypt: Commando voor het decrypten.
* -inkey: De key file die gebruikt wordt voor het encrypten (default private key). In dit geval de private key van de ontvanger.
* -in: Input van file name.
* -out: wegschrijven van de file.

![White space](/readme-images/white.png)

## 6. Signing
Een belangrijke stap bij het versturen van berichten is het valideren van de afzender. Kan je er zeker van zijn dat de afzender aangeeft wie hij is? Om dit probleem op te lossen kunnen we gebruik maken van signing.

Signing is het aanmaken van een digitale handtekening voor een data bestand. Dit word gedaan om aan te tonen dat het bestand authentiek is, dat de zender ook echt de zender is. Het signen wordt dan ook gedaan door middel van de private key van de zender. Alleen de zender kan die handtekening zetten. Het veriferen van de handtekening wordt door de ontvanger gedaan door middel van de public key van de zender.

De flow voor het signen van een data bestand:

![White space](/readme-images/signing.png)

#### 6a. Zorg er nu voor dat je bericht gesigned wordt met jouw private key
Zoals we bij stap 5 een bericht hebben encrypt gaan we bij deze stap een bericht signen met jouw private key. Ook hiervoor maken we weer gebruik van openssl met het rsautl commando. Dit gesignde bericht kan je dan met iemand delen die jouw public key bezit (Die is als het goed is al gedeeld bij stap 5).

Commando voor het signen van data:

`openssl rsautl -sign -inkey private-[naam].pem -in data.txt -out [ontvanger].txt.sign`

* rsautl: Commando voor het signen / veriferen / en crypten en decrypten van data.
* -sign: Commando voor het signen.
* -inkey: De key file die gebruikt wordt voor het encrypten (default private key). In dit geval de private key van de zender.
* -in: Input van file name.
* -out: wegschrijven van de file


#### 6b. Verify iemand anders zijn gesignede bericht.
Probeer bij deze stap iemand zijn gesignde bericht te verifen. Dit doe je door de public key te gebruiken van de verzender van het bericht.

`openssl rsautl -verify -pubin -inkey public-[naam].pem -in [ontvanger].txt.sign  -out data.txt`

* rsautl: Commando voor het signen / veriferen / en crypten en decrypten van data.
* -verify: Commando voor het veriferen.
* -inkey: De key file die gebruikt wordt voor het encrypten (default private key). In dit geval de public key van de zender.
* -pubin: Aangeven dat de input een public RSA key is.
* -in: Input van file name.
* -out: wegschrijven van de file

### 6.1. Complete flow van sign en hash data bestanden
Voor een complete flow van signen maken we eerst een hash van het bericht. Deze hash kunnen we dan signen met onze private key. Hierna wordt het bericht en de gesignde hash naar de ontvanger gestuurd. De ontvanger kan dan de gesignde hash veriferen, het bericht zelf hashen en dan de geverifeerde hash en de eigen hash vergelijken. Als er een verschil tussen beide hashes is dan heeft iemand het bericht aangepast. De integriteit is hierbij geschonden.

Er kan ook nog voor worden gekozen om dit als geheel te encrypten als het om vertrouwbare data gaat.

Flow voor het hashen en signen van een data bestand:

![White space](/readme-images/signing-full.png)